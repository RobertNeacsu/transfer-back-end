### Urzeala crocodilească
*(Adică tema ta de transfer)*
## Salut!
**Dacă ai ajuns până în acest punct, înseamnă că dorești să devii o reptilă.**

Pentru ca dorința ta să fie îndeplinită, mai ai un obstacol de învins frate.

Stai! Mai întăi de toate, trebuie să configurezi niște lucruri fără de care nu te poți descurca:
 - [XAMPP](https://www.apachefriends.org/index.html) - Important pentru a putea porni un server local și un server mySQL.
 - [Postman](https://www.postman.com/downloads/) - Important pentru a putea să testezi rutele pe care le vei implementa(Fiind back-end și dezvoltând back-end, se presupune că nu ai încă interfața grafica de front-end).
 - [VSCode](https://code.visualstudio.com/) - Noi folosim VSCode în mare parte și asta e recomandarea noastră. Dacă ești familiarizat cu alt IDE, nu este nicio problemă.
 - [Git](https://git-scm.com/downloads) - Pentru o versionare ușoară și o platforma unde poți să-ți ții codul safe.

 Acum, câte ceva pentru a putea să te apuci de treabă.
 - [Node.js](https://nodejs.org/en/download/) - Instaleaza node.js
 > 1. Clonează repo-ul primit și deschide-l într-un proiect VSCode.
 > 2. Deschide un terminal din meniul de navigare de sus.
 > 3. Rulează comanda: **npm install** pentru a instala toate dependințele prin care aplicația poate rula.
 > 4. Rulează comanda: **nodemon** pentru a porni server-ul de back-end.

 **Notă:** În cazul în care întâmpini erori la rularea comenzii **nodemon** rulează **npm start** sau **node server.js**.
 --
 **Nu uita** să iți creezi o bază de date cu numele 'masinute' accesând butonul Admin de langa serverul pornit de mySQL din interfata XAMPP.
 > După ce ai creat baza de date cu numele corespunzător, intra în browserul tău și accesează link-ul: 'http://localhost:8080/reset' pentru ca tabelele necesare să fie create.
 
**Trebuie să rezolvi următoarea provocare:**

Ești gata?
**Să-i dăm bice!**

>*Niște cioroi doresc să își comande mașini printr-o aplicație web.
Tu, o posibilă reptilă din back-end trebuie să te asiguri că datele lor ajung în cea mai bună formă în baza noastră de date.*

Pentru asta ai de rezolvat **următoarele cerințe:**

 ##### 1. O rută POST pentru adăugarea unei mașini în baza de date:
 Asigură-te că datele introduse prin POST sunt validate. **Cioroii nu trebuie să introducă date la mișto.**
> a) validare câmpuri non-empty

> b) validare email(@***.com)

> c) validare număr telefon(10 caractere si incepe cu 07)

> d) culoare (fără valori numerice)

> e) capacitate cilindrica(o singură cifră după virgulă și doar valori numerice)

> f) anul fabricatiei(mai nou de 1900)

> g) impozitul se calculează în funcție de capacitatea cilindrica (pentru 1.0 = 100 lei și la fiecare 0.2 creste cu 50 lei) și se introduce in câmpul corespunzator

> h) asigurarea se calculează și se introduce în baza de date ca 7% din prețul mașinii.

> **Important:**
Testeaza ruta creată prin adăugarea a mai multor mașini de aceeași marcă(ex: 3 toyota, 2 audi, 1 logan).
Asigură-te că ai și mașini din 2000-2005.
---
 ##### 2. O rută PUT pentru modificarea prețului unei mașini:
 --
 > La modificarea prețului, asigurarea trebuie bineînțeles recalculată în funcție de modificarea prețului la apelul acestei rute.
 ---
 
  ##### 3. O rută GET pe mașini:
  --
  > Ruta trebuie să returneze mașinile de o anumită marcă la alegere.
  
 ##### 4. O rută DELETE pe mașini:
 --
 > Ruta trebuie să șteargă toate mașinile fabricate mai vechi de 2004.
 ---
  ##### 5. ULTIMUL PAS IMPORTANT:
  --
  > Fă-ți un cont pe git în caz în care nu ai deja și adaugă ce ai meșterit cu drag și spor pentru a vedea și noi tot ce ai meșterit!
  Un mic ajutor în caz în care nu te descurci la acest pas: [AICI](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html?fbclid=IwAR0f_I5RQ49iCC4zVnYTuJaxazx1A3vIFmq7SU513_5jTq7lrkD9cOmY4lY). Mai departe ne vei da prin email un link către acesta alături de motivația scrisă și un filmuleț în care vreți să ne arăți "De ce vreți să fii în IT?". Te rugăm să trimiți pe oricare din adresele munteanu.cristian1009@gmail.com sau robertneacsu2000@gmail.com

  *Probleme la implementare?*
  **Contacts -> Departments -> IT -> Tuca Madalin/Robert Neacsu/Mariana Serban**

  Mult spor! 
  (Și nu uita, Google is your friend).
 
 
 
