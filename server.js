const express = require("express");
const bodyParser = require("body-parser");
const server = express();
server.use(bodyParser.json());
const PORT = 8080;

server.listen(PORT, () => {
  console.log("Server-ul ruleaze pe: " + PORT);
});

const connection = require("./bdconfig").connection;
//Ruta pentru resetarea bazei de date
server.get("/reset", async (req, res) => {
  connection
    .sync({
      force: true,
    })
    .then(() => {
      res.status(200).send({
        message: "Baza de date resetata!",
      });
    })
    .catch(() => {
      res.status(500).send({
        message: "Server error!",
      });
    });
});

//De implementat rutele din cerinta
